from __future__ import annotations

from pytest import mark


def test_dict_simple():
    """Testing basic dict operations"""
    DCT_KEY = "hey"
    DCT_VALUE = "hi"

    # Created dict is empty
    dct = dict()
    assert len(dct) == 0
    assert DCT_KEY not in dct
    assert dct.get(DCT_KEY) is None
    try:
        assert dct[DCT_KEY] is None
    except KeyError:
        pass

    # New key can be added
    dct[DCT_KEY] = DCT_VALUE
    assert len(dct) == 1
    assert DCT_KEY in dct
    assert dct[DCT_KEY] == DCT_VALUE
    assert dct.get(DCT_KEY) == DCT_VALUE

    # Added keys can be deleted
    assert dct.pop(DCT_KEY) == DCT_VALUE
    assert dct.get(DCT_KEY) is None
    assert DCT_KEY not in dct
    assert len(dct) == 0


@mark.parametrize(
    ("keys", "length"),
    [
        ([], 0),
        (["hey", "hi"], 2),
        (["hey", "hey"], 1),
    ],
)
def test_dict_parametrized(keys: list[str], length: int):
    """Testing dict lengths with different keys (including duplicates)"""
    DCT_VALUE = "some_value"

    dct1 = dict.fromkeys(keys, DCT_VALUE)
    dct2 = {k: DCT_VALUE for k in keys}

    assert len(dct1) == length
    assert len(dct2) == length
    for key in keys:
        assert key in dct1
        assert key in dct2
    assert dct1.items() == dct2.items()


def test_dict_comprehension():
    """Ridiculous test for dict comprehension"""
    LENGTH = 5  # >= 2
    KEYS = tuple(range(LENGTH))
    VALUES = tuple(range(LENGTH))

    dct = {
        KEYS[i]: VALUES[j]
        for i in range(LENGTH)
        for j in range(LENGTH)
        if i != j
    }

    for i, key in enumerate(KEYS):
        assert key in dct
        if i == LENGTH - 1:
            assert dct[key] == VALUES[-2]
        else:
            assert dct[key] == VALUES[-1]


def test_set_basic():
    """Testing adding items and checking uniqueness"""
    LENGTH = 5  # >= 2

    test_set = set(range(LENGTH))
    assert len(test_set) == LENGTH
    for i in range(LENGTH):
        assert i in test_set
        test_set.add(i)
        assert len(test_set) == LENGTH

    test_set.clear()
    assert len(test_set) == 0
    for i in range(LENGTH):
        assert i not in test_set


@mark.parametrize(
    ("left", "right", "diff_left", "diff_right"),
    [
        ([1, 2, 3], [1, 2, 3], [], []),  # two identical sets
        ([1, 2, 3], [4, 5, 6], [1, 2, 3], [4, 5, 6]),  # no overlap
        ([1, 2, 3], [2, 3, 4], [1], [4]),  # some elements overlap
        ([1, 1, 1], [1, 2, 2], [], [2]),  # left is subset of right
    ],
)
def test_set_difference(
    left: list[int],
    right: list[int],
    diff_left: list[int],
    diff_right: list[int],
):
    """Testing set differences, see test cases"""
    left_set = set(left)
    right_set = set(right)

    left_minus_right = left_set.difference(right_set)
    right_minus_left = right_set.difference(left_set)

    assert len(left_minus_right) == len(diff_left)
    assert len(right_minus_left) == len(diff_right)

    for element in diff_left:
        assert element in left_minus_right
    for element in diff_right:
        assert element in right_minus_left


def test_set_ordering():
    """Set items are sorted, the original order is lost"""
    ITEMS = (1, 4, 5, 3, 2)
    SORTED_ITEMS = sorted(ITEMS)

    test_set = set()
    for item in ITEMS:
        test_set.add(item)
        assert item in test_set

    for item in SORTED_ITEMS:
        assert test_set.pop() == item
